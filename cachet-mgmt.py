#!/usr/bin/env python3
# Example usage:
# List all components:
#   ./cachet-mgmt.py list
# List incidents
#   ./cachet-mgmt.py list-incident
# Create maintenance:
#   ./cachet-mgmt.py maintenance "Scheduled maintenance for upgrade" "27/10/2024 00:00 UTC"
# Create incident:
#   ./cachet-mgmt.py incident "Multiple services affected" "A network outage is being investigated"
# Create incident update:
#   ./cachet-mgmt.py add-incident-update <incident_id> <message> <status> where <status> is:
#   1 = Investigating, 2 = Identified, 3 = Watching, 4 = Fixed
# List incident updated
#   ./cachet-mgmt.py list-incident-update <incident_id>
# Delete incident
#  ./cachet-mgmt.py remove-incident <incident_id>
# Update incident
#  ./cachet-mgmt.py update-incident <incident_id> <status> where <status> is:
#  1 = Operational, 2 = Performance Issues, 3 = Partial Outage, 4 = Major Outage

import os
import argparse
import json
import sys
from datetime import datetime, timedelta
import requests
from tabulate import tabulate
from typing import Dict, List, Optional

class CachetAPI:
    """
    CachetAPI handles communication with the Cachet API.
    """

    def __init__(self, base_url: str, api_token: str):
        self.base_url = base_url.rstrip('/')
        self.headers = {
            'X-Cachet-Token': api_token,
            'Content-Type': 'application/json'
        }

    def _make_request(self, method: str, endpoint: str, data: Optional[Dict] = None) -> requests.Response:
        """
        Make an HTTP request to the Cachet API with error handling.
        """
        url = f"{self.base_url}{endpoint}"
        try:
            response = requests.request(
                method=method,
                url=url,
                headers=self.headers,
                json=data
            )
            response.raise_for_status()
            return response
        except requests.exceptions.RequestException as e:
            print(f"Error making request to {url}: {str(e)}")
            if hasattr(e.response, 'json'):
                try:
                    print("API Error:", json.dumps(e.response.json(), indent=2))
                except:
                    print("Raw response:", e.response.text)
            sys.exit(1)

    def list_components(self) -> List[Dict]:
        """
        Retrieve all components.
        """
        response = self._make_request('GET', '/api/components')
        return response.json().get('data', [])

    def list_incidents(self) -> List[Dict]:
        """
        Retrieve all incidents with their details.
        """
        response = self._make_request('GET', '/api/incidents')
        return response.json().get('data', [])

    def create_maintenance(self, message: str, scheduled_at: datetime) -> Dict:
        """
        Create a scheduled maintenance event.
        """
        scheduled_at = parse_datetime(scheduled_at)
        scheduled_to = scheduled_at + timedelta(hours=2)

        data = {
            "name": "Scheduled Maintenance",
            "message": message,
            "status": 0,
            "visible": 1,
            "scheduled_at": scheduled_at.strftime("%Y-%m-%d %H:%M:%S"),
            "scheduled_to": scheduled_to.strftime("%Y-%m-%d %H:%M:%S"),
        }

        response = self._make_request('POST', '/api/schedules', data)
        return response.json()

    def create_incident(self, name: str, message: str) -> Dict:
        """
        Create a new incident.
        """
        data = {
            "name": name,
            "message": message,
            "status": 1,
            "visible": 1,
            "component_status": 4,
        }

        response = self._make_request('POST', '/api/incidents', data)
        return response.json()

    def add_incident_update(self, incident_id: int, message: str, status: int) -> Dict:
        """
        Add an update to an existing incident.
        """
        data = {
            "message": message,
            "status": status,
        }

        response = self._make_request('POST', f'/api/incidents/{incident_id}/updates', data)
        return response.json()

    def list_incident_updates(self, incident_id: int) -> List[Dict]:
        """
        List all updates for a specific incident.
        """
        response = self._make_request('GET', f'/api/incidents/{incident_id}/updates')
        return response.json().get('data', [])

    def remove_incident(self, incident_id: int) -> None:
        """
        Remove an incident by its ID.
        """
        self._make_request('DELETE', f'/api/incidents/{incident_id}')

    def update_incident(self, incident_id: int, status: int, message: Optional[str] = None) -> Dict:
        """
        Update the status and optionally the message of an incident.
        """
        data = {"status": status}
        if message:
            data["message"] = message
        response = self._make_request('PUT', f'/api/incidents/{incident_id}', data)
        return response.json()

def parse_datetime(datetime_str: str) -> datetime:
    """
    Parse a datetime string in the format 'DD/MM/YYYY HH:MM UTC'.
    """
    return datetime.strptime(datetime_str, "%d/%m/%Y %H:%M %Z")

def setup_arguments() -> argparse.ArgumentParser:
    """
    Set up command-line arguments for the script.
    """
    parser = argparse.ArgumentParser(description='Cachet Management CLI')

    subparsers = parser.add_subparsers(dest='command', help='Command to execute')

    # Commands
    subparsers.add_parser('list', help='List all components')
    subparsers.add_parser('list-incident', help='List all incidents')

    maintenance_parser = subparsers.add_parser('maintenance', help='Create maintenance')
    maintenance_parser.add_argument('message', help='Maintenance message')
    maintenance_parser.add_argument('scheduled_at', help='Scheduled at (DD/MM/YYYY HH:MM UTC)')

    incident_parser = subparsers.add_parser('incident', help='Create incident')
    incident_parser.add_argument('name', help='Service name(s) affected')
    incident_parser.add_argument('message', help='Incident message')

    update_parser = subparsers.add_parser('update-incident', help='Update an incident status')
    update_parser.add_argument('incident_id', type=int, help='Incident ID')
    update_parser.add_argument('status', type=int, help='New status')
    update_parser.add_argument('--message', help='Optional update message')

    remove_parser = subparsers.add_parser('remove-incident', help='Remove an incident')
    remove_parser.add_argument('incident_id', type=int, help='Incident ID')

    list_updates_parser = subparsers.add_parser('list-incident-updates', help='List updates for an incident')
    list_updates_parser.add_argument('incident_id', type=int, help='Incident ID')

    add_update_parser = subparsers.add_parser('add-incident-update', help='Add an update to an incident')
    add_update_parser.add_argument('incident_id', type=int, help='Incident ID')
    add_update_parser.add_argument('message', help='Update message')
    add_update_parser.add_argument('status', type=int, help='New status for the update')

    return parser

def format_data(data: List[Dict], headers: List[str]) -> str:
    """
    Generic function to format data for display in a tabulated format.
    """
    status_map = {
        1: "Operational",
        2: "Performance Issues",
        3: "Partial Outage",
        4: "Major Outage"
    }

    formatted_data = []
    for item in data:
        row = {}
        for header in headers:
            if header == "status":
                status = item['attributes'].get(header, "N/A")
                if isinstance(status, dict):
                    row[header] = status.get('human', "Unknown")
                elif isinstance(status, int):
                    row[header] = status_map.get(status, "Unknown")
                else:
                    row[header] = "Unknown"
            else:
                row[header] = item['attributes'].get(header, "N/A")
        formatted_data.append(row)

    return tabulate(formatted_data, headers="keys", tablefmt="grid")

def main():
    parser = setup_arguments()
    args = parser.parse_args()

    if not args.command:
        parser.print_help()
        sys.exit(1)

    api_url = os.getenv('CACHET_API_URL')
    api_token = os.getenv('CACHET_API_TOKEN')

    if not api_url or not api_token:
        print("Error: CACHET_API_URL and CACHET_API_TOKEN environment variables must be set.")
        sys.exit(1)

    api = CachetAPI(api_url, api_token)

    try:
        if args.command == 'list':
            components = api.list_components()
            print(format_data(components, ["id", "name", "status"]))

        elif args.command == 'list-incident':
            incidents = api.list_incidents()
            print(format_data(incidents, ["id", "name", "message", "status"]))

        elif args.command == 'maintenance':
            result = api.create_maintenance(args.message, args.scheduled_at)
            print("Maintenance created successfully!")

        elif args.command == 'incident':
            result = api.create_incident(args.name, args.message)
            print("Incident created successfully!")

        elif args.command == 'update-incident':
            result = api.update_incident(args.incident_id, args.status, args.message)
            print("Incident updated successfully!")

        elif args.command == 'remove-incident':
            api.remove_incident(args.incident_id)
            print(f"Incident {args.incident_id} removed successfully!")

        elif args.command == 'list-incident-updates':
            updates = api.list_incident_updates(args.incident_id)
            print(format_data(updates, ["id", "message", "status", "created_at"]))

        elif args.command == 'add-incident-update':
            result = api.add_incident_update(args.incident_id, args.message, args.status)
            print("Incident update added successfully!")

    except Exception as e:
        print(f"Error while generating result: {e}")
        sys.exit(1)

if __name__ == '__main__':
    main()
