#!/usr/bin/python3

import gitlab
import os

GROUP_ID = 4001

gl = gitlab.Gitlab('https://gitlab.gnome.org', os.getenv('GITLAB_PRIVATE_RW_TOKEN'), api_version=4)

g = gl.groups.get(GROUP_ID)
p = g.projects.list(archived=False)

for project in p:
  proj = gl.projects.get(project.id)
  proj.archive()
