builds_with_artifacts = Ci::Build.with_downloadable_artifacts
builds_to_clear = builds_with_artifacts.where("finished_at < ?", 1.month.ago)
builds_to_clear.find_each do |build|
  build.artifacts_expire_at = Time.now
  build.erase_erasable_artifacts!
end
