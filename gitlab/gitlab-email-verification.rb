admin = User.where(admin:true).first

# any user with only snippets is a spammer
User.active.
  where(["id NOT IN (select distinct(author_id) from notes where noteable_type != ?)", "Snippet"]).
  where('id NOT IN (select distinct(author_id) from issues)').
  where('id NOT IN (select distinct(user_id) from subscriptions)').
  where('id NOT IN (select distinct(user_id) from project_authorizations)').
  where('id IN (select distinct(author_id) from snippets)').each do |user|
    puts "destroy for spam in snippets #{user.username}";
    Users::DestroyService.new(admin).execute(user)
  end

# any user with only snippet comments is a spammer
User.active.
  where('id NOT IN (select distinct(author_id) from issues)').
  where('id NOT IN (select distinct(user_id) from subscriptions)').
  where('id NOT IN (select distinct(user_id) from project_authorizations)').
  where(["id NOT IN (select distinct(author_id) from notes where noteable_type != ?)", "Snippet"]).
  where(["id IN (select distinct(author_id) from notes where noteable_type = ?)", "Snippet"]).
  each do |user|
    puts "destroy for comment spam in snippets #{user.username}";
    Users::DestroyService.new(admin).execute(user)
  end

# automatically ban users reported by GNOME members for spam or phishing
AbuseReport.open.each { |report|
  next if not ["spam", "phishing"].include?(report.category)
  # don't ban ghost and service desk users
  next if report.user_id.nil?
  next if report.user_id == 51
  next if report.user_id == 39851

  reporter = User.find(report.reporter_id)
  reporter_groups = reporter.groups
  reporter_groups.each { |group|
    if group.name == "GNOME"
      reported_user = User.find(report.user_id)
      Users::BanService.new(admin).execute(reported_user)
      report.closed!
      break
    end
  }
}

# increase projects limit for users with ssh, gpg or 2fa configured
configured_users = []

User.active.
  where('projects_limit = 0').
  where('id IN (select distinct(user_id) from keys)').
  each() { |user|
    next if configured_users.include? user
    configured_users << user
  }

User.active.
  where('projects_limit = 0').
  where('id IN (select distinct(user_id) from gpg_keys)').
  each() { |user|
    next if configured_users.include? user
    configured_users << user
  }

User.active.
  where('projects_limit = 0').
  where('encrypted_otp_secret IS NOT NULL').
  each() { |user|
    next if configured_users.include? user
    configured_users << user
  }

configured_users.each do |user|
  puts "increase projects limit for #{user.username}"
  user.projects_limit = 50
  user.external=false
  user.save!
end

# clean up users who:
# - have no project
# - are not part of any group
# - created no MR (how could have they?)
# - not participating in any comment
# - not participating in any issue
# - where not active in the past 14 days
# - have no notifcation set up
User.active.
  where('id NOT IN (select distinct(user_id) from project_authorizations)').
  where('id NOT IN (select distinct(author_id) from merge_requests)').
  where(["id NOT IN (select distinct(author_id) from notes where noteable_type != ?)", "Snippet"]).
  where('id NOT IN (select distinct(author_id) from issues)').
  where('id NOT IN (select distinct(user_id) from subscriptions)').
  where("last_activity_on <= ?", 7.days.ago).
  each() { |user|
    if user.notification_settings.count == 0
      then
      puts "delete unused account #{user.username}"
      Users::DestroyService.new(admin).execute(user)
    end
  }

# same here except the user never logged in and was created > 14 days ago
User.active.
  where('id NOT IN (select distinct(user_id) from project_authorizations)').
  where('id NOT IN (select distinct(author_id) from merge_requests)').
  where(["id NOT IN (select distinct(author_id) from notes where noteable_type != ?)", "Snippet"]).
  where('id NOT IN (select distinct(author_id) from issues)').
  where('id NOT IN (select distinct(user_id) from subscriptions)').
  where("last_activity_on IS NULL").
  where("created_at <= ?", 7.days.ago).
  each() { |user|
    if user.notification_settings.count == 0
      then
      puts "delete unused account #{user.username}"
      Users::DestroyService.new(admin).execute(user)
    end
  }

# block users who posted spam and use fake user agent with Windows mentioned
SpamLog.where("updated_at >= ?", 20.minutes.ago).each() { |spam|
  spam_user = User.find(spam.user_id)
  next if spam_user.banned?
  if spam.user_agent.include? "Windows NT"
    if spam_user.created_at >= 1.days.ago
      puts "ban potential spammer #{spam_user.username}"
      Users::BanService.new(admin).execute(spam_user)
    end
  end
}

# remove users matching the deny list
filter = /reinblut1818/i
User.active.
  where("created_at <= ?", 7.days.ago).each() { |user|
    if user.name.match(filter) or user.username.match(filter) or user.email.match(filter)
      Users::BanService.new(admin).execute(user)
    end
  }
