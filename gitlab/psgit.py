#!/usr/bin/python

import psutil
import time

for p in psutil.process_iter():
    name = p.name()
    cmdline = p.cmdline()

    if len(cmdline) > 2 and cmdline[1] == "upload-pack":
        repo = "/".join(cmdline[-1].split('/')[6:])
        age = int(time.time()) - int(p.create_time())
        pid = p.pid

        print("{} {} {}".format(pid, age, repo))
