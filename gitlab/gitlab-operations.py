#!/usr/bin/python3

import sys
import gitlab
import re
import os

sys.path.append('/home/admin/bin')
sys.path.append('/home/admin/bin/git')
import gnome_ldap_utils as Glu
import semi_rdf

from xml.sax import SAXParseException
from urllib.error import HTTPError

LDAP_GROUP_BASE, LDAP_HOST, LDAP_USER_BASE, LDAP_USER, LDAP_PASSWORD, GITLAB_PRIVATE_RW_TOKEN = os.getenv('LDAP_GROUP_BASE'), \
os.getenv('LDAP_HOST'), os.getenv('LDAP_USER_BASE'), os.getenv('LDAP_USER'), os.getenv('LDAP_PASSWORD'), \
os.getenv('GITLAB_PRIVATE_RW_TOKEN')

glu = Glu.Gnome_ldap_utils(LDAP_GROUP_BASE, LDAP_HOST, LDAP_USER_BASE, LDAP_USER, LDAP_PASSWORD)
gl = gitlab.Gitlab('https://gitlab.gnome.org', GITLAB_PRIVATE_RW_TOKEN, api_version=4)

DOAP = "http://usefulinc.com/ns/doap#"
GNOME = "http://api.gnome.org/doap-extensions#"

committers = glu.get_uids_from_group('gnomecvs')
committers_exceptions = glu.get_uids_from_group('gnomecvs_exceptions')
_committers = dict()
for uid in committers:
    try:
        _committers[uid] = gl.users.list(extern_uid=(f"uid={ uid },{ LDAP_USER_BASE }"), \
            provider='ldapmain')[0].id
    except IndexError:
        continue

_committers_exceptions = dict()
for uid in committers_exceptions:
    try:
        _committers_exceptions[uid] = gl.users.list(extern_uid=(f"uid={ uid },{ LDAP_USER_BASE }"), \
            provider='ldapmain')[0].id
    except IndexError:
        continue

gnome_group = gl.groups.get(8, with_projects=False)
gnome_group_users = { i.username : i.id for i in gnome_group.members.list(get_all=True) }
gnome_projects = gnome_group.projects.list(get_all=True)
gnome_subgroups = gnome_group.subgroups.list()
for subgroup in gnome_subgroups:
    s = gl.groups.get(subgroup.id)
    subprojects = s.projects.list()
    gnome_projects.extend(subprojects)

l10n_group = gl.groups.get(13336, with_projects=False)

for username, id in _committers.items():
    if id not in gnome_group_users.values():
        gnome_group.members.create({'user_id': id, 'access_level': gitlab.const.AccessLevel.DEVELOPER})
        print(f"Account with username { username } and with id { id } has been added to the GNOME group")

        try:
            l10n_group.members.create({'user_id': id, 'access_level': gitlab.const.AccessLevel.REPORTER})
            print(f"Account with username { username } and id { id } has been added to the Teams/Translation group")
        except gitlab.exceptions.GitlabCreateError as e:
            if e.response_code == 409:
                pass

for username, id in gnome_group_users.items():
    if id not in _committers.values() and id not in _committers_exceptions.values():
        role = gnome_group.members.get(id).access_level
        if role != 20 and id != 1:
            gnome_group.members.delete(id)
            print(f"Account with username { username } with id { id } has been removed from the GNOME group")

        try:
            l10n_group.members.delete(id)
            print(f"Account with username { username } and id { id } has been removed from the Teams/Translation group")
        except gitlab.exceptions.GitlabDeleteError as e:
            if e.response_code == 404:
                pass

maints = dict()
for project in gnome_projects:
    project_full_path = project.attributes['path_with_namespace']
    project_name = project.attributes['path']
    uids = []

    doap_url = 'https://gitlab.gnome.org/%s/raw/%s/%s.doap'
    default_branch = project.attributes.get('default_branch')
    if not default_branch:
        default_branch = 'master'

    # Ensure stable branches are protected
    # Ensure all tags are protected (required by GNOME Release Service)
    try:
        proj = gl.projects.get(project.id)
        branches = [branch.name for branch in proj.branches.list(get_all=True)]
        stable_branches_regex = re.compile(r'(gnome-\d-\d\d|gnome-\d\d)')
        stable_branches = set(filter(stable_branches_regex.match, branches))
        protected_branches = [branch.name for branch in proj.protectedbranches.list(get_all=True)]

        if default_branch not in protected_branches:
            proj.protectedbranches.create({
                'name': default_branch,
                'merge_access_level': gitlab.const.AccessLevel.DEVELOPER,
                'push_access_level': gitlab.const.AccessLevel.DEVELOPER
            })

        if len(stable_branches) > 0 and 'gnome-*' not in protected_branches:
            proj.protectedbranches.create({
                'name': 'gnome-*',
                'merge_access_level': gitlab.const.AccessLevel.DEVELOPER,
                'push_access_level': gitlab.const.AccessLevel.DEVELOPER
            })

        protected_tags = [tag.name for tag in proj.protectedtags.list(get_all=True)]

        if '*' not in protected_tags:
            proj.protectedtags.create({
                'name': '*',
                'allowed_to_create': [{'access_level': 30}],
                'create_access_level': 30
            })
    except gitlab.exceptions.GitlabListError as e:
        # Some projects are not used for git hosting
        if e.response_code == 404:
            pass

    # Ensure container registry is enabled for GNOME projects
    proj.container_registry_enabled = True
    proj.save()

    try:
        nodes = semi_rdf.read_rdf(doap_url % (project_full_path, default_branch, project_name))
    except (SAXParseException, HTTPError):
        nodes = ''

    for node in nodes:
      if node.name != (DOAP, "Project"):
        continue

      for role in [u'maintainer', u'helper']:
          for maint in node.find_properties((DOAP, role)):
              if not isinstance(maint, semi_rdf.Node):
                continue

              uid = maint.find_property((GNOME, u'userid'))
              if not isinstance(uid, str):
                continue

              uid = str(uid)
              uids.append(uid)

              maints[project_full_path] = uids

for project in maints:
    try:
        proj = gl.projects.get('%s' % project)
    except gitlab.exceptions.GitlabGetError as e:
        if e.response_code == 404:
            continue

    for user in maints[project]:
        userid = None
        if user in _committers.keys():
            userid = _committers[user]
        else: # fallback for GNOME accounts holders who are not members of the gnomecvs group
           try:
               userid_ldap = glu.ldap_user_exists(userid)
           except AttributeError:
               userid_ldap = None

           if userid_ldap:
               userid_gl = gl.users.list(extern_uid=(f"uid={ uid },{ LDAP_USER_BASE }"), provider='ldapmain')
               if len(userid_gl):
                   userid = userid_gl[0].id

        if userid:
            try:
                proj.members.create({'user_id': userid, 'access_level': gitlab.const.AccessLevel.MAINTAINER})

                print(f"Landed master level access to { user } against repository { project }")
            except gitlab.exceptions.GitlabCreateError as e:
                if e.response_code == 409:
                    if proj.members.get(userid).access_level != 40:
                        proj.members.delete(userid)
                        proj.members.create({'user_id': userid, 'access_level': gitlab.const.AccessLevel.MAINTAINER})

                        print(f"Landed master level access to { user } against repository { project }")

    members = proj.members.list()
    members_dict = {}

    for member in members:
        identity_found = False
        user = gl.users.get(member.attributes['id'])

        if len(user.attributes['identities']) > 0:
            for index, _ in enumerate(user.attributes['identities']):
                provider = user.attributes['identities'][index]['provider']
                if provider == 'ldapmain':
                    members_dict[user.attributes['identities'][index]['extern_uid'].split(',')[0].replace('uid=', '')] = user.attributes['id']
                    identity_found = True

            if not identity_found:
                members_dict[user.attributes['username']] = user.attributes['id']
        else:
            members_dict[user.attributes['username']] = user.attributes['id']

    for member in members_dict:
        if member not in maints[project]:
            _member = proj.members.get(members_dict[member])
            if _member.attributes['access_level'] == 40:
                if not _member.attributes['username'].startswith(f"project_{ proj.attributes['id'] }_bot"):
                    proj.members.delete(members_dict[member])

                    print(f"Dropped master level access to { member } against repository { project } as maintainer entry is missing on the DOAP file")
            elif _member.attributes['access_level'] == 20:
                pass
            else:
                if not _member.attributes['username'].startswith(f"project_{ proj.attributes['id'] }_bot"):
                    proj.members.delete(members_dict[member])

                    print(f"Dropped level access { _member.attributes['access_level'] }, this means user { member } was added manually on project { project }, that is not necessary as permissions are inherited from the GNOME group by default")
