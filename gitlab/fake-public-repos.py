#!/usr/include/python

import os
import gitlab

GITLAB_TOKEN = os.getenv("GITLAB_TOKEN")
if GITLAB_TOKEN is None:
    with open("/home/admin/secret/gitlab_rw") as f:
        tokenfile = f.readline()
    GITLAB_TOKEN = tokenfile.rstrip().split("=")[1]

gl = gitlab.Gitlab(
    "https://gitlab.gnome.org", private_token=GITLAB_TOKEN, per_page=100
)
gl.auth()

projects = gl.projects.list(all=True)
for project in projects:
    attrs = project.attributes
    if attrs['visibility'] != 'public':
        continue

    issues_access_level = attrs['issues_access_level'] == 'private'
    repository_access_level = attrs['repository_access_level'] == 'private'
    merge_requests_access_level = attrs['merge_requests_access_level'] == 'private'

    if all([issues_access_level, repository_access_level, merge_requests_access_level]):
        print project.path_with_namespace
