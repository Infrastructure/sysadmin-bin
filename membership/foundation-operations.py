#!/usr/bin/python3

import socket
import smtplib
import string
import sys
import os
import datetime as dt

from email.mime.text import MIMEText
from optparse import OptionParser

sys.path.append('/home/admin/bin')
import gnome_ldap_utils as Glu

usage = "usage: %prog [options]"
parser = OptionParser(usage)

parser.add_option("--send-form-letters",
                  action="store_true", default=False,
                  help="Checks for LastRenewedOn / FirstAdded fields and send "
                       "welcome emails to new or existing Foundation members accordingly")
parser.add_option("--automatic-subscriptions",
                  action="store_true", default=False,
                  help="Automatically subscribes new Foundation members to the foundation-announce "
                       "mailing list. To be executed on smtp.gnome.org")
parser.add_option("--remove-expired-foundation-members",
                  action="store_true", default=False,
                  help="Foundation membership lasts two years, remove expired members from the "
                       "foundation LDAP group")
parser.add_option("--generate-membership-list",
                  action="store_true", default=False,
                  help="Generate and publish the Foundation membership list that will appear at "
                       "https://www.gnome.org/foundation/membership")

(options, args) = parser.parse_args()

if len(sys.argv) == 1:
    parser.print_help()
    sys.exit(1)

try:
    exec(open('/home/admin/secret/freeipa_rw_foundation').read())
except IOError as e:
    if e.errno == 2:
        LDAP_GROUP_BASE = os.environ.get('LDAP_GROUP_BASE')
        LDAP_USER_BASE = os.environ.get('LDAP_USER_BASE')
        LDAP_HOST = os.environ.get('LDAP_HOST')
        LDAP_USER = os.environ.get('LDAP_USER')
        LDAP_PASSWORD = os.environ.get('LDAP_PASSWORD')
        LDAP_CA_PATH = os.environ.get('LDAP_CA_PATH')

glu = Glu.Gnome_ldap_utils(LDAP_GROUP_BASE, LDAP_HOST, LDAP_USER_BASE, LDAP_USER, LDAP_PASSWORD)


def send_renewal_emails():
    today = str(dt.date.today())

    foundationmembers = glu.get_uids_from_group('foundation')

    for member in foundationmembers:
        ldap_vars = glu.get_attributes_from_ldap(member, 'FirstAdded', 'LastRenewedOn', 'mail', 'cn')

        if (ldap_vars[1] and ldap_vars[2]):
            if (ldap_vars[2] == today) and (ldap_vars[1] == today):
                send_form_letters(new_member_form_letter, ldap_vars[3], ldap_vars[4], ldap_vars[0])
            elif ldap_vars[2] == today:
                send_form_letters(renewal_form_letter, ldap_vars[3], ldap_vars[4])


def rtdelta_wrap(t, a):
    import dateutil.relativedelta as relativedelta
    if t == 'years':
        return relativedelta.relativedelta(years=a)
    elif t == 'months':
        return relativedelta.relativedelta(months=a)
    elif t == 'days':
        return relativedelta.relativedelta(days=a)


def cleanup_mailgrace():
    today = dt.date.today()

    gracemails = glu.get_uids_from_group('mailgrace')
    foundationmembers = glu.get_uids_from_group('foundation')

    for member in gracemails:
       ldap_vars = glu.get_attributes_from_ldap(member, 'LastRenewedOn', 'mail', 'cn')
       last_renewed_on = dt.datetime.strptime(ldap_vars[1], '%Y-%m-%d').date()

       if member in foundationmembers:
          glu.remove_user_from_ldap_group(member, 'mailgrace')
       else:
           if today >= (last_renewed_on + rtdelta_wrap('years', 2) + rtdelta_wrap('months', 3)):
               send_form_letters(alias_decomm, ldap_vars[2], ldap_vars[3], ldap_vars[1])
               glu.remove_user_from_ldap_group(member, 'mailgrace')


def create_discourse_post(content, title, category):
    from pydiscourse import DiscourseClient

    api_username = os.getenv('DISCOURSE_API_USERNAME')
    api_key = os.getenv('DISCOURSE_API_KEY')

    client = DiscourseClient(
        'https://discourse.gnome.org',
        api_username=api_username,
        api_key=api_key
    )
    topic = client.create_post(
        content,
        title=title,
        category_id=category
    )

    return topic


def remove_expired_memberships_from_foundation():
    today = dt.date.today()

    members_list = ''

    foundationmembers = glu.get_uids_from_group('foundation')

    for member in foundationmembers:
        ldap_vars = glu.get_attributes_from_ldap(member, 'LastRenewedOn', 'mail', 'cn')
        last_renewed_on = dt.datetime.strptime(ldap_vars[1], '%Y-%m-%d').date()

        if today == (last_renewed_on + rtdelta_wrap('years', 2) - rtdelta_wrap('months', 1)):
           send_form_letters(close_to_expire_membership_form_letter, ldap_vars[2], ldap_vars[3], ldap_vars[1])
        elif (today - rtdelta_wrap('years', 2)) >= last_renewed_on:
            print(f"Removing { member } from the foundation LDAP group as the membership expired on { ldap_vars[1] }")
            glu.remove_user_from_ldap_group(member, 'foundation')
            glu.add_user_to_ldap_group(member, 'mailgrace')
            send_form_letters(expired_membership_form_letter, ldap_vars[2], ldap_vars[3], ldap_vars[1])

        if (last_renewed_on.year == (today - rtdelta_wrap('years', 2)).year) and (last_renewed_on.month == today.month) and (today.day == 1):
           members_list += ldap_vars[3] + ', ' + ldap_vars[1] + '\n'

    if len(members_list) > 0:
      title = "Memberships needing renewal ({}-{})".format(today.year, today.month)
      category_id = os.getenv('DISCOURSE_CATEGORY_ID')
      content = renewals_to_foundation_list.safe_substitute(expired_members=members_list)

      create_discourse_post(content, title, category_id)


def generate_membership_list():
    import json

    foundationmembers = glu.get_uids_from_group("foundation")
    emeritus = glu.get_uids_from_group("emeritus")

    members_list = []
    emeritus_list = []
    result = {}

    for member in foundationmembers:
        ldap_vars = glu.get_attributes_from_ldap(member, "cn", "LastRenewedOn")

        members_list.append(ldap_vars[1])
        members_list.sort(key=str.casefold)

    for member in emeritus:
        ldap_vars = glu.get_attributes_from_ldap(member, "cn", "LastRenewedOn")

        emeritus_list.append(ldap_vars[1])
        emeritus_list.sort(key=str.casefold)

    result['members'] = [{"name": name} for name in members_list]
    result['emeritus'] = [{"name": name} for name in emeritus_list]

    with open("memberslist.json", "w") as f:
        json.dump(result, f)


def send_form_letters(form_letter, email, name, *args):
    today = dt.date.today()

    year_month = str(today.year) + '-' + str(today.month)

    try:
        if form_letter is new_member_form_letter:
            msg = MIMEText(form_letter.safe_substitute(cn=name,
                           uid=args[0]), 'plain', 'utf8')
        elif form_letter is renewal_form_letter:
            msg = MIMEText(form_letter.safe_substitute(cn=name),
                           'plain', 'utf8')
        elif form_letter in (expired_membership_form_letter, close_to_expire_membership_form_letter,
                             alias_decomm):
            msg = MIMEText(form_letter.safe_substitute(cn=name,
                           last_renewed_on_date=args[0]), 'plain', 'utf8')

        if form_letter is alias_decomm:
            msg['Subject'] = "Your @gnome.org alias has been decommissioned"
        else:
            msg['Subject'] = "Your GNOME Foundation Membership"
        msg['From'] = "GNOME Foundation Membership Committee <noreply@gnome.org>"
        msg['To'] = "%s" % (email)
        server = smtplib.SMTP(os.getenv('SMTP_HOST', default='localhost'))
        server.sendmail(msg['From'], [msg['To']], msg.as_string())
        server.quit()
        print(f"Successfully sent mail with subject \"{ msg['subject'] }\" to { name } with email { email }")
    except smtplib.SMTPException:
        print("ERROR: I wasn't able to send the email correctly, please check /var/log/maillog!")


def subscribe_new_members():
    if socket.gethostname() != 'restaurant.gnome.org':
        sys.exit("This function should only be used on restaurant.gnome.org")

    today = str(dt.date.today())

    foundationmembers = glu.get_uids_from_group('foundation')
    members = []

    for member in foundationmembers:
        ldap_vars = glu.get_attributes_from_ldap(member, 'FirstAdded', 'LastRenewedOn')

        if today in (ldap_vars[1], ldap_vars[2]):
            members.append(ldap_vars[0])
        else:
            continue

    if len(members) > 0:
        for member in members:
            with open('/tmp/new_subscribers', 'w') as fl:
                fl.write(f'{ member }@gnome.org' + '\n')

            import subprocess
            subscribe = subprocess.Popen(['/usr/lib/mailman/bin/add_members', '-a', 'n', '-r', '/tmp/new_subscribers', 'foundation-announce'])
            subscribe.wait()
            os.remove('/tmp/new_subscribers')


new_member_form_letter = string.Template("""
Dear $cn,

Congratulations, you are now a member of the GNOME Foundation! Welcome, and
thank you for supporting GNOME. Your name has joined those of the rest of the
Foundation Membership:

   https://www.gnome.org/foundation/membership

As a member of the Foundation, you are able to vote in the elections of the
Board of Directors, and you can also put yourself forward as a candidate for
the Board. There are many other benefits to being a member, including having
your blog on Planet GNOME, a @gnome.org email address, an Owncloud account,
your own blog hosted within the GNOME Infrastructure and the ability to apply
for travel subsidies. A full list of the benefits and the guidelines to obtain
them is available at:

   https://wiki.gnome.org/MembershipCommittee/MembershipBenefits

While all the available benefits can be obtained on demand by looking at the above
URL, @gnome.org email aliases are automatically created within 24 hours from the
arrival of this email on your INBOX. Instructions for correctly setting up your
email alias can be found at the following link:

   https://wiki.gnome.org/Infrastructure/MailAliasPolicy#Mail_aliases_configuration

If you are interested in managing your Foundation Membership information (including
where your @gnome.org alias is supposed to forward to) and you didn't have a GNOME
Account before today, an account has been created for you. You should now be able to
reset your password at:

   https://password.gnome.org

Please note your username is $uid.

And login at:

   https://account.gnome.org

To help you stay informed about GNOME Foundation events, we have subscribed you
to the foundation-announce mailing list, where all the major GNOME Foundation
announcements are sent. It is a low volume list and does not allow subscribers
to post emails. If you would like to read the archives you can do so here:

    https://mail.gnome.org/mailman/listinfo/foundation-announce

We also encourage you to subscribe to the foundation-list mailing
list. It is used to discuss any issue relating to the GNOME Foundation. This is
the place for you to suggest ideas and voice your opinions on issues pertaining
to the GNOME Foundation. To subscribe or read the archives, go to:

    https://mail.gnome.org/mailman/listinfo/foundation-list

We also highly encourage you to introduce yourself to the members by
writing to foundation-list. Please list your contributions and write a
little about yourself, if you like :-)

We have a map of contributors on our wiki. If you want others to be able to find
you, you may add yourself to the list. This might be a good opportunity to find
other contributors in your area:

    https://wiki.gnome.org/GnomeWorldWide

For more information about the GNOME Foundation, visit the GNOME Foundation's
web page at:

    https://www.gnome.org/foundation

Thank you for all your great work as a member of the GNOME community.

Best wishes,

The GNOME Foundation Membership Committee,
https://discourse.gnome.org/c/community/membership/338""")

renewal_form_letter = string.Template("""
Dear $cn,

We are pleased to inform you that your GNOME Foundation Membership has
been renewed for two years.

Thank you for your ongoing contributions to GNOME, and for continuing to
support the GNOME Foundation.

You are eligible to become a candidate for election and to vote in the annual
Board of Directors elections held each June before GUADEC. If you were not
already subscribed to the foundation-announce mailing list, you have been
subscribed to this list, where all the major GNOME Foundation announcements are
sent. It is a low volume list and does not allow subscribers to post emails. If
you would like to read the archives you can do so here:

    https://mail.gnome.org/mailman/listinfo/foundation-announce

You are also encouraged to subscribe to the foundation-list mailing list. It is
open to the public (even non-members) and is used to discuss any issue relating
to the GNOME Foundation. This is the place for you to suggest ideas and voice
your opinions on issues pertaining to the GNOME Foundation. To subscribe or
read the archives, go to:

    https://mail.gnome.org/mailman/listinfo/foundation-list

For more information about the GNOME Foundation, visit the GNOME Foundation's
web page at:

    https://www.gnome.org/foundation

Thanks for your contributions to GNOME.

Best wishes,

The GNOME Foundation Membership Committee,
https://discourse.gnome.org/c/community/membership/338""")

expired_membership_form_letter = string.Template("""
Dear $cn,

from our records it seems your GNOME Foundation Membership was last renewed
on $last_renewed_on_date (YYYY-MM-DD) and its duration is currently set to be two years.

If you want to continue being a member of the GNOME Foundation please make sure to submit
a renewal request at https://foundation.gnome.org/membership/apply. If you did
so already, please ignore this e-mail and wait for the GNOME Foundation Membership
Committee to get back to you.

More details on when your membership was last renewed can be found on your profile page
at https://account.gnome.org under the 'Last Renewed on date' field.

In the case you feel your contributions would not be enough for the membership
renewal to happen you can apply for the Emeritus status:

  https://wiki.gnome.org/MembershipCommittee/EmeritusMembers

Additionally, please give a look at the Membership benefits:

  https://wiki.gnome.org/MembershipCommittee/MembershipBenefits

On a related note, we'd like you to remember your @gnome.org alias will also
be decommisioned within 3 months from your membership expiration.

Thanks,
  The GNOME Membership and Elections Committee,
  https://discourse.gnome.org/c/community/membership/338""")

alias_decomm = string.Template("""
Dear $cn,

from our records it seems your GNOME Foundation Membership was last renewed
3 months ago on $last_renewed_on_date (YYYY-MM-DD). As we previously communicated
your @gnome.org alias has been decommissioned as a direct consequence of your lack of
interest in being part of the GNOME Foundation anymore. If you'd like your alias to be
activated again please make sure you renew or apply for Emeritus membership at:

  https://www.gnome.org/foundation/membership/apply

Thanks,
  The GNOME Membership and Elections Committee,
  https://discourse.gnome.org/c/community/membership/338""")

close_to_expire_membership_form_letter = string.Template("""
Dear $cn,

from our records it seems your GNOME Foundation Membership is about to expire within
four (4) weeks starting from today as it was las renewed on $last_renewed_on_date (YYYY-MM-DD).
Membership duration is currently set to be two years.

If you want to continue being a member of the GNOME Foundation please make sure to submit
a renewal request at https://foundation.gnome.org/membership/apply. If you did
so already, please ignore this e-mail and wait for the GNOME Foundation Membership
Committee to get back to you.

More details on when your membership was last renewed can be found on your profile page
at https://account.gnome.org under the 'Last Renewed on date' field.

In the case you feel your contributions would not be enough for the membership
renewal to happen you can apply for the Emeritus status:

  https://wiki.gnome.org/MembershipCommittee/EmeritusMembers

Additionally, please give a look at the Membership benefits:

  https://wiki.gnome.org/MembershipCommittee/MembershipBenefits

Thanks,
  The GNOME Membership and Elections Committee,
  https://discourse.gnome.org/c/community/membership/338""")

renewals_to_foundation_list = string.Template("""
Hi,

as per point 1.3 of [1], here it comes a list of members in need of a
renew in case they didn't receive their individual e-mail:

First name Last name, (Last renewed on)

$expired_members

The Renewal form can be found at [2].

Cheers,
   GNOME Membership and Elections Committee,
   https://discourse.gnome.org/c/community/membership/338

[1] https://mail.gnome.org/archives/foundation-list/2011-November/msg00000.html
[2] http://www.gnome.org/foundation/membership/apply/

""")


def main():
    if options.send_form_letters:
        send_renewal_emails()

    if options.automatic_subscriptions:
        subscribe_new_members()

    if options.remove_expired_foundation_members:
        remove_expired_memberships_from_foundation()
        cleanup_mailgrace()

    if options.generate_membership_list:
        generate_membership_list()


if __name__ == "__main__":
    main()
